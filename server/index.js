const express = require("express");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 5000;
const { MongoClient, ServerApiVersion, ObjectId } = require("mongodb");
const nodemailer = require("nodemailer");

require("dotenv").config();

app.use(cors());
app.use(express.json());

// const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.6v5oj5d.mongodb.net/?retryWrites=true&w=majority`;s
// const uri = `mongodb+srv://RohanChhetri:Natsudragkneel12@cluster0.sdgel1y.mongodb.net/`;
const uri = "mongodb+srv://Rangdol:12210088@clusteromni.4frz9eh.mongodb.net/";
console.log(uri);
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1,
});

async function run() {
  try {
    const heroTextCollection = client.db("omniverse").collection("heroSection");
    const trail = client.db("omniverse").collection("trial");
    const aboutsCollection = client
      .db("omniverse")
      .collection("aboutsCollection");
    const previousCollection = client
      .db("omniverse")
      .collection("previousCollection");
    const blogCollection = client.db("omniverse").collection("blogCollection");
    const courseCollection = client
      .db("omniverse")
      .collection("courseCollection");
    const omniyuaCourseCollection = client
      .db("omniverse")
      .collection("omniyuaCourseCollection");
    const projectCollection = client
      .db("omniverse")
      .collection("projectCollection");
    const supportWorkCollection = client
      .db("omniverse")
      .collection("supportWork");

    const videoCollection = client
      .db("omniverse")
      .collection("videoCollection");
    const shoonyaVideoCollection = client
      .db("omniverse")
      .collection("shoonyaVideoCollection");
    const bookCollection = client.db("omniverse").collection("bookCollection");
    const uniteTextCollection = client.db("omniverse").collection("uniteText");
    const stewardTextCollection = client
      .db("omniverse")
      .collection("stewardText");
    const trialData = client.db("omniverse").collection("trial-data");
    const locationCollection = client
      .db("omniverse")
      .collection("location-data");
    const fundingCollection = client.db("omniverse").collection("funding-data");
    const volunteerCollection = client
      .db("omniverse")
      .collection("volunteer-data");
    const mediaVideoCollection = client
      .db("omniverse")
      .collection("media-videos");
    const mediaImageCollection = client
      .db("omniverse")
      .collection("media-images");
    const networkCollection = client.db("omniverse").collection("network-data");
    const expertiseCollection = client
      .db("omniverse")
      .collection("expertise-data");

    //------------------------------ Hero section ------------------------//
    // get hero section text
    app.get("/hero-section", async (req, res) => {
      const query = {};
      const heroText = await heroTextCollection.find(query).toArray();
      res.send(heroText);
    });

    //update hero title
    app.put("/update-hero-title/:id", async (req, res) => {
      const id = req.params.id;
      const update = req.body;
      const filter = { _id: new ObjectId(id) };
      const option = { upsert: true };
      const updateDoc = {
        $set: {
          title: update.title,
        },
      };

      const result = await heroTextCollection.findOneAndUpdate(
        filter,
        updateDoc,
        option
      );
      res.send(result);
    });

    //update hero title
    app.put("/update-para-one/:id", async (req, res) => {
      const id = req.params.id;
      const update = req.body;
      const filter = { _id: new ObjectId(id) };
      const option = { upsert: true };
      const updateDoc = {
        $set: {
          paraOne: update.paraOne,
        },
      };

      const result = await heroTextCollection.findOneAndUpdate(
        filter,
        updateDoc,
        option
      );
      res.send(result);
    });

    //update hero title
    app.put("/update-para-two/:id", async (req, res) => {
      const id = req.params.id;
      const update = req.body;
      const filter = { _id: new ObjectId(id) };
      const option = { upsert: true };
      const updateDoc = {
        $set: {
          paraTwo: update.paraTwo,
        },
      };

      const result = await heroTextCollection.findOneAndUpdate(
        filter,
        updateDoc,
        option
      );
      res.send(result);
    });

    //-------------------------------Blog / article---------------------------------//

    // post of blogs
    app.post("/post-blog", async (req, res) => {
      const blogBody = req.body;
      const result = await blogCollection.insertOne(blogBody);
      res.send(result);
    });

    // get all blog/article
    app.get("/all-blogs", async (req, res) => {
      const query = {};
      const allBlog = await blogCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allBlog);
    });

    // delete article
    app.delete("/delete-blog/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await blogCollection.deleteOne(query);
      res.send(result);
    });

    // update article info
    app.put("/updated-blog-info/:id", async (req, res) => {
      const id = req.params.id;
      const { updatedTitle, updatedLink, updatedDescription } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            title: updatedTitle,
            link: updatedLink,
            description: updatedDescription,
          },
        };

        const result = await blogCollection.findOneAndUpdate(filter, updateDoc);

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    //-------------------------------------- Courses -------------------------------------//

    // get all courses
    app.get("/all-courses", async (req, res) => {
      const query = {};
      const allBlog = await courseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allBlog);
    });
    app.get("/get-course-status/:isUpcoming", async (req, res) => {
      const isUpcoming = req.params.isUpcoming === "true";
      let query = { isUpcoming: isUpcoming };
      const result = await courseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });

    //delete course
    app.delete("/delete-course/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await courseCollection.deleteOne(query);
      res.send(result);
    });

    //get course  details by dynamic url
    app.get("/omni-life/course-details/:dynamicUrl", async (req, res) => {
      const dynamicUrl = req.params.dynamicUrl;
      const query = { dynamicUrl: dynamicUrl };
      const courses = await courseCollection.findOne(query);
      res.send(courses);
    });
    //get course  details by dynamic id
    app.get("/omni-life/course-detail/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const courses = await courseCollection.findOne(query);
      res.send(courses);
    });

    //------------------------------------- Project ----------------------------------//

    //get project
    app.get("/all-projects", async (req, res) => {
      const query = {};
      const allProject = await projectCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allProject);
    });

    // update project info
    app.put("/updated-project-info/:id", async (req, res) => {
      const id = req.params.id;
      const { updatedTitle, updatedLink, updatedDescription } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            projectTitle: updatedTitle,
            projectLink: updatedLink,
            description: updatedDescription,
          },
        };

        const result = await projectCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res
            .status(200)
            .json({ message: "Project information updated successfully" });
        } else {
          res.status(404).json({ message: "Project not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    // post project
    app.post("/post-project", async (req, res) => {
      const projectBody = req.body;
      const result = await projectCollection.insertOne(projectBody);
      res.send(result);
    });

    //delete project
    app.delete("/delete-project/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await projectCollection.deleteOne(query);
      res.send(result);
    });

    //---------------------- Support our work --------------------------//
    //get homepage text
    app.get("/support-work", async (req, res) => {
      const query = {};
      const supportWork = await supportWorkCollection.find(query).toArray();
      res.send(supportWork);
    });

    //update support work
    app.put("/update-support-work/:id", async (req, res) => {
      const id = req.params.id;
      const update = req.body;
      const filter = { _id: new ObjectId(id) };
      const option = { upsert: true };
      const updateDoc = {
        $set: {
          text: update.text,
        },
      };
      const result = await supportWorkCollection.updateOne(
        filter,
        updateDoc,
        option
      );
      res.send(result);
    });

    //-------------------- Unite section-----------------------//
    //get unite text
    app.get("/unite-text", async (req, res) => {
      const query = {};
      const timeSkill = await uniteTextCollection.find(query).toArray();
      res.send(timeSkill);
    });

    //update unite text

    app.put("/update-unite-text/:id", async (req, res) => {
      const id = req.params.id;
      const update = req.body;
      const filter = { _id: new ObjectId(id) };
      const option = { upsert: true };
      const updateDoc = {
        $set: {
          uniteText: update.uniteText,
        },
      };
      const result = await uniteTextCollection.updateOne(
        filter,
        updateDoc,
        option
      );
      res.send(result);
    });

    //--------------------------- Steward section --------------------------//
    //get steward text
    app.get("/steward-text", async (req, res) => {
      const query = {};
      const timeSkill = await stewardTextCollection.find(query).toArray();
      res.send(timeSkill);
    });

    //update steward text

    app.put("/update-steward-text/:id", async (req, res) => {
      const id = req.params.id;
      const update = req.body;
      const filter = { _id: new ObjectId(id) };
      const option = { upsert: true };
      const updateDoc = {
        $set: {
          stewardText: update.stewardText,
        },
      };
      const result = await stewardTextCollection.updateOne(
        filter,
        updateDoc,
        option
      );
      res.send(result);
    });

    //--------------------------- About section -------------------------------//
    //get about collection data
    app.get("/get-about-info/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const courses = await aboutsCollection.findOne(query);
      res.send(courses);
    });

    //updated  -hindi-text
    app.put("/updated-hindi-text/:id", async (req, res) => {
      const id = req.params.id;
      const updatedHindiText = req.body.updatedHindiText;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { hindiText: updatedHindiText },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //updated  -moral-text
    app.put("/updated-moral-text/:id", async (req, res) => {
      const id = req.params.id;
      const updatedMoralText = req.body.updatedMoralText;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { moralText: updatedMoralText },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });

    //update name
    app.put("/updated-name/:id", async (req, res) => {
      const id = req.params.id;
      const updatedName = req.body.updatedName;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { name: updatedName },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    // update course image
    app.put("/updated-about-image/:id", async (req, res) => {
      const id = req.params.id;
      const { updatedImage } = req.body;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { aboutImage: updatedImage },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update description
    app.put("/updated-about-description/:id", async (req, res) => {
      const id = req.params.id;
      const updatedDescription = req.body.updatedDescription;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { description: updatedDescription },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update middle text
    app.put("/updated-about-middle-text/:id", async (req, res) => {
      const id = req.params.id;
      const updatedMiddleText = req.body.updatedMiddleText;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { middleText: updatedMiddleText },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update steward title
    app.put("/updated-about-steward-title/:id", async (req, res) => {
      const id = req.params.id;
      const updatedStewardTitle = req.body.updatedStewardTitle;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { stewardTitle: updatedStewardTitle },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update steward text
    app.put("/updated-about-steward-text/:id", async (req, res) => {
      const id = req.params.id;
      const updatedStewardText = req.body.updatedStewardText;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { stewardText: updatedStewardText },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //updated steward names
    app.put("/updated-about-steward-names/:id", async (req, res) => {
      const id = req.params.id;
      const updatedStewardNames = req.body.updatedStewardNames;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { stewardNames: updatedStewardNames },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update guiding title
    app.put("/updated-about-guiding-title/:id", async (req, res) => {
      const id = req.params.id;
      const updatedGuidingTitle = req.body.updatedGuidingTitle;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { guidingTitle: updatedGuidingTitle },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update guiding text
    app.put("/updated-about-guiding-text/:id", async (req, res) => {
      const id = req.params.id;
      const updatedGuidingText = req.body.updatedGuidingText;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { guidingText: updatedGuidingText },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });

    //updated guiding names
    app.put("/updated-about-guiding-names/:id", async (req, res) => {
      const id = req.params.id;
      const updatedGuidingNames = req.body.updatedGuidingNames;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { guidingNames: updatedGuidingNames },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update aligned title
    app.put("/updated-about-aligned-title/:id", async (req, res) => {
      const id = req.params.id;
      const updatedAlignedTitle = req.body.updatedAlignedTitle;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { alignedTitle: updatedAlignedTitle },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //updated aligned names
    app.put("/updated-about-aligned-names/:id", async (req, res) => {
      const id = req.params.id;
      const updatedAlignedNames = req.body.updatedAlignedNames;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { alignedNames: updatedAlignedNames },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update sangha title
    app.put("/updated-about-sangha-title/:id", async (req, res) => {
      const id = req.params.id;
      const updatedSanghaTitle = req.body.updatedSanghaTitle;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { sanghaTitle: updatedSanghaTitle },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //update sangha text
    app.put("/updated-about-sangha-text/:id", async (req, res) => {
      const id = req.params.id;
      const updatedSanghaText = req.body.updatedSanghaText;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { sanghaText: updatedSanghaText },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });

    //update sangha names
    app.put("/updated-about-sangha-names/:id", async (req, res) => {
      const id = req.params.id;
      const updatedSanghaNames = req.body.updatedSanghaNames;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { sanghaNames: updatedSanghaNames },
      };
      const result = await aboutsCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });

    // --------------------------- Update omnilife courses -----------------------//

    // create new course object create-new-omniyua-course
    app.post("/create-new-object", (req, res) => {
      const { newObject } = req.body;
      const result = courseCollection.insertOne(newObject);
      res.send(result);
    });

    //make event false
    app.put("/make-isEvent-false/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsEvent = req.body.changeIsEvent;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isEvent: changeIsEvent },
      };
      const result = await courseCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //make event false
    app.put("/make-isEvent-true/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsEvent = req.body.changeIsEvent;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isEvent: changeIsEvent },
      };
      const result = await courseCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //get course for event
    app.get("/get-course/:isEvent", async (req, res) => {
      const isEvent = req.params.isEvent === "true";
      let query = { isEvent: isEvent };
      const result = await courseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });
    // check isEvent from two collection
    app.get("/get-every-course/:isEvent", async (req, res) => {
      try {
        const isEvent = req.params.isEvent === "true";
        const query = { isEvent: isEvent };
        const omniyaCourses = await omniyuaCourseCollection
          .find(query)
          .sort({ _id: -1 })
          .toArray();
        const normalCourses = await courseCollection
          .find(query)
          .sort({ _id: -1 })
          .toArray();
        const allCourses = [...omniyaCourses, ...normalCourses];
        res.send(allCourses);
      } catch (error) {
        console.error("Error fetching courses:", error);
        res.status(500).send("Error fetching courses");
      }
    });
    //make upcoming false
    app.put("/make-isUpcoming-false/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsUpcoming = req.body.changeIsUpcoming;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isUpcoming: changeIsUpcoming },
      };
      const result = await courseCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //make publiush
    app.put("/make-omnilife-publish/:id", async (req, res) => {
      const id = req.params.id;
      const makePublish = req.body.makePublish;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isEvent: makePublish,
          isUpcoming: makePublish,
        },
      };
      const result = await courseCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //make unpubliush
    app.put("/make-omnilife-unpublish/:id", async (req, res) => {
      const id = req.params.id;
      const makeunPublish = req.body.makeunPublish;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isEvent: makeunPublish,
          isUpcoming: makeunPublish,
        },
      };
      const result = await courseCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    //add new other information
    app.put("/omnilife/add-new-other-information/:id", async (req, res) => {
      const id = req.params.id;
      const newData = req.body.newData;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          otherInfos: newData,
        },
      };
      const result = await courseCollection.findOneAndUpdate(filter, updateDoc);
      res.send(result);
    });
    // -----------------------------------Shoonya course -------------------------//
    //get shoonya video
    app.get("/get-shoonya-videos", async (req, res) => {
      const query = {};
      const shoonyaVideo = await shoonyaVideoCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(shoonyaVideo);
    });
    // post shoonya-video
    app.post("/post-shoonya-video", async (req, res) => {
      const addVideo = req.body;
      const result = await shoonyaVideoCollection.insertOne(addVideo);
      res.send(result);
    });
    //delete shoonya video
    app.delete("/delete-shoonya-video/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await shoonyaVideoCollection.deleteOne(query);
      res.send(result);
    });
    // update shoonya video information
    app.put("/updated-shoonya-info/:id", async (req, res) => {
      const id = req.params.id;
      const { updatedTitle, updatedLink } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            videoTitle: updatedTitle,
            videoLink: updatedLink,
          },
        };

        const result = await shoonyaVideoCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });
    //--------------------- Previous course/experience collection -----------------//
    // post previous experience
    app.post("/post-previous-course", async (req, res) => {
      const previousCourse = req.body;
      const result = await previousCollection.insertOne(previousCourse);
      res.send(result);
    });

    app.get("/get-all-previous-courses", async (req, res) => {
      let query = {};
      const result = await previousCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });
    //-----------------------------------Omniyua course ------------------------//
    //get omniyua course
    app.get("/get-omniyua-course", async (req, res) => {
      const query = {};
      const omniyua = await omniyuaCourseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(omniyua);
    });
    //get course by isPublish
    app.get("/get-omniyua-course/:isPublish", async (req, res) => {
      const isPublish = req.params.isPublish === "true";
      let query = { isPublish: isPublish };
      const result = await omniyuaCourseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });
    //  create-new-omniyua-course
    app.post("/create-new-omniyua-course", (req, res) => {
      const { newObject } = req.body;
      const result = omniyuaCourseCollection.insertOne(newObject);
      res.send(result);
    });
    //make publish false
    app.put("/make-isPublish-false/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsEvent = req.body.changeIsEvent;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: changeIsEvent,
          isEvent: changeIsEvent,
        },
      };
      const result = await omniyuaCourseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make publish true
    app.put("/make-isPublish-true/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsEvent = req.body.changeIsEvent;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: changeIsEvent },
      };
      const result = await omniyuaCourseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make event false
    app.put("/make-omniyua-isEvent-false/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsEvent = req.body.changeIsEvent;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isEvent: changeIsEvent },
      };
      const result = await omniyuaCourseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make  event false
    app.put("/make-omniyua-isEvent-true/:id", async (req, res) => {
      const id = req.params.id;
      const changeIsEvent = req.body.changeIsEvent;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isEvent: changeIsEvent },
      };
      const result = await omniyuaCourseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //get course  details by id
    app.get("/omniyua/course-details-id/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const courses = await omniyuaCourseCollection.findOne(query);
      res.send(courses);
    });

    //get course  details by url
    app.get("/omni-yua/course-details/:dynamicUrl", async (req, res) => {
      const dynamicUrl = req.params.dynamicUrl;
      const query = { dynamicUrl: dynamicUrl };
      const courses = await omniyuaCourseCollection.findOne(query);
      res.send(courses);
    });
    //delete omniyua course
    app.delete("/delete-omniyua-course/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await omniyuaCourseCollection.deleteOne(query);
      res.send(result);
    });

    //add new other information
    app.put("/omniyua/add-new-other-information/:id", async (req, res) => {
      const id = req.params.id;
      const newData = req.body.newData;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          otherInfos: newData,
        },
      };
      const result = await omniyuaCourseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //----------------------------- Update omniyua courses -----------------------//

    // update project information
    app.put("/api/update-omniyua-course/:id", async (req, res) => {
      const id = req.params.id;
      const {
        updatedImg,
        updatedCourseName,
        updatedCourseTitle,
        updatedButton,
        updatedDate,
        updatedDynamicUrl,
        updatedOtherInfo,
        editorHtml,
        isPublish,
        isEvent,
      } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            courseName: updatedCourseName,
            courseTitle: updatedCourseTitle,
            otherInfos: updatedOtherInfo,
            date: updatedDate,
            button: updatedButton,
            dynamicUrl: updatedDynamicUrl,
            content: editorHtml,
            isPublish,
            isEvent,
          },
        };

        const result = await omniyuaCourseCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });
    // ---------------------------Update omnilife course------------------------//
    app.put("/api/update-omnilife-course/:id", async (req, res) => {
      const id = req.params.id;
      const {
        updatedImg,
        updatedCourseName,
        updatedCourseTitle,
        updatedButton,
        updatedDate,
        updatedDynamicUrl,
        updatedOtherInfo,
        editorHtml,
        isUpcoming,
        isEvent,
      } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            courseName: updatedCourseName,
            courseTitle: updatedCourseTitle,
            otherInfos: updatedOtherInfo,
            date: updatedDate,
            button: updatedButton,
            dynamicUrl: updatedDynamicUrl,
            content: editorHtml,
            isUpcoming,
            isEvent,
          },
        };

        const result = await courseCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    // -----------------------------------Resource -------------------------------//
    //get all video
    app.get("/all-resource-video", async (req, res) => {
      const query = {};
      const allVideo = await videoCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allVideo);
    });
    // post resource video
    app.post("/post-resource-video", async (req, res) => {
      const addVideo = req.body;
      const result = await videoCollection.insertOne(addVideo);
      res.send(result);
    });
    //delete resource video
    app.delete("/delete-resource-video/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await videoCollection.deleteOne(query);
      res.send(result);
    });

    //delete resource book
    app.delete("/delete-resource-book/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await bookCollection.deleteOne(query);
      res.send(result);
    });
    //get all book
    app.get("/all-resource-book", async (req, res) => {
      const query = {};
      const allBook = await bookCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allBook);
    });
    // post new book
    app.post("/post-resource-book", async (req, res) => {
      const addBook = req.body;
      const result = await bookCollection.insertOne(addBook);
      res.send(result);
    });

    /////  trail
    //get about data

    //get all traial data
    app.get("/all-try-course", async (req, res) => {
      const query = {};
      const data = await trialData.find(query).toArray();
      res.send(data);
    });

    app.get("/try-data/:videoTitle", async (req, res) => {
      const videoTitle = req.params.videoTitle;
      const query = { videoTitle: videoTitle };
      const courses = await trialData.findOne(query);
      res.send(courses);
    });
    //
    app.get("/get-trial-data/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const courses = await trialData.findOne(query);
      res.send(courses);
    });

    //react quill

    app.get("/get-quill-data/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const courses = await trail.findOne(query);
      res.send(courses);
    });

    //-------------------------------------- Pilot Locations -------------------------------------//
    // create new course object create-new-omniyua-course
    app.post("/create-new-location", (req, res) => {
      const { newObject } = req.body;
      const result = locationCollection.insertOne(newObject);
      res.send(result);
    });

    // get all Pilot Locations
    app.get("/all-location", async (req, res) => {
      const query = {};
      const allLocation = await locationCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allLocation);
    });

    //delete location
    app.delete("/delete-location/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await locationCollection.deleteOne(query);
      res.send(result);
    });
    //add new other information
    app.put("/add-new-location-information/:id", async (req, res) => {
      const id = req.params.id;
      const newData = req.body.newData;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          otherInfos: newData,
        },
      };
      const result = await locationCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //get locaation  details by dynamic url
    app.get("/location-details/:dynamicUrl", async (req, res) => {
      const dynamicUrl = req.params.dynamicUrl;
      const query = { dynamicUrl: dynamicUrl };
      const locations = await locationCollection.findOne(query);
      res.send(locations);
    });
    //get location  details by dynamic id
    app.get("/location-detail/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const locations = await locationCollection.findOne(query);
      res.send(locations);
    });

    //get location by isPublish
    app.get("/get-pilot-location/:isPublish", async (req, res) => {
      const isPublish = req.params.isPublish === "true";
      let query = { isPublish: isPublish };
      const result = await locationCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });

    //make publish false
    app.put("/make-location-isPublish-false/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await locationCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make publish true
    app.put("/make-location-isPublish-true/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await locationCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    app.put("/api/update-location/:id", async (req, res) => {
      const id = req.params.id;
      const {
        updatedImg,
        updatedLocationName,
        updatedOtherInfo,
        editorHtml,
        updatedDynamicUrl,
      } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            locationName: updatedLocationName,
            otherInfos: updatedOtherInfo,
            content: editorHtml,
            dynamicUrl: updatedDynamicUrl,
          },
        };

        const result = await locationCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    //-------------------------------------- Funding & Volunteering -------------------------------------//
    // create new funding object
    app.post("/create-new-funding", (req, res) => {
      const { newObject } = req.body;
      fundingCollection
        .insertOne(newObject)
        .then((result) => {
          // Send a meaningful response back to the client
          res
            .status(201)
            .json({ message: "Funding created successfully", result });
        })
        .catch((error) => {
          console.error("Error creating new funding:", error);
          res.status(500).json({ message: "Internal server error" });
        });
    });
    // create new volunteer object
    app.post("/create-new-voluntering", (req, res) => {
      const { newObject } = req.body;
      volunteerCollection
        .insertOne(newObject)
        .then((result) => {
          // Send a meaningful response back to the client
          res
            .status(201)
            .json({ message: "Funding created successfully", result });
        })
        .catch((error) => {
          console.error("Error creating new funding:", error);
          res.status(500).json({ message: "Internal server error" });
        });
    });

    // get all fundings
    app.get("/all-fundings", async (req, res) => {
      const query = {};
      const allFunding = await fundingCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allFunding);
    });
    // get all volunteers
    app.get("/all-volunterings", async (req, res) => {
      const query = {};
      const allVolunteers = await volunteerCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allVolunteers);
    });

    //get funding by isPublish
    app.get("/get-funding/:isPublish", async (req, res) => {
      const isPublish = req.params.isPublish === "true";
      let query = { isPublish: isPublish };
      const allFunding = await fundingCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allFunding);
    });

    //get volunteering by isPublish
    app.get("/get-volunteering/:isPublish", async (req, res) => {
      const isPublish = req.params.isPublish === "true";
      let query = { isPublish: isPublish };
      const allVolunteers = await volunteerCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allVolunteers);
    });

    //make funding publish false
    app.put("/unpublish-funding/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await fundingCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make funding publish true
    app.put("/publish-funding/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await fundingCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //make volunteering publish false
    app.put("/unpublish-volunteering/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await volunteerCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make volunteering publish true
    app.put("/publish-volunteering/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await volunteerCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //delete funding
    app.delete("/delete-funding/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await fundingCollection.deleteOne(query);
      res.send(result);
    });

    //delete volunteer
    app.delete("/delete-voluntering/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await volunteerCollection.deleteOne(query);
      res.send(result);
    });

    //add new other funding information
    app.put("/add-new-funding-information/:id", async (req, res) => {
      const id = req.params.id;
      const newData = req.body.newData;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          otherInfos: newData,
        },
      };
      const result = await fundingCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //add new other volunteer information
    app.put("/add-new-voluntering-information/:id", async (req, res) => {
      const id = req.params.id;
      const newData = req.body.newData;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          otherInfos: newData,
        },
      };
      const result = await volunteerCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //update-volunteer
    app.put("/api/update-voluntering/:id", async (req, res) => {
      const id = req.params.id;
      const {
        updatedImg,
        updatedVolunteringName,
        updatedOtherInfo,
        updatedButtonName,
      } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            volunteringName: updatedVolunteringName,
            otherInfos: updatedOtherInfo,
            buttonName: updatedButtonName,
          },
        };

        const result = await volunteerCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });
    //get funding  details by dynamic id
    app.get("/funding-detail/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const fundings = await fundingCollection.findOne(query);
      res.send(fundings);
    });

    //get voluntering  details by dynamic id
    app.get("/voluntering-detail/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const voluntering = await volunteerCollection.findOne(query);
      if (voluntering) {
        res.send(voluntering);
      } else {
        res.status(404).send("Voluntering not found");
      }
    });
    //update funding
    app.put("/api/update-funding/:id", async (req, res) => {
      const id = req.params.id;
      const {
        updatedImg,
        updatedFundingName,
        updatedOtherInfo,
        updatedButtonName,
      } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            fundingName: updatedFundingName,
            otherInfos: updatedOtherInfo,
            buttonName: updatedButtonName,
          },
        };

        const result = await fundingCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });
    //update funding
    app.put("/api/update-volutntering/:id", async (req, res) => {
      const id = req.params.id;
      const {
        updatedImg,
        updatedVolunteringName,
        updatedOtherInfo,
        updatedButtonName,
      } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            volunteerName: updatedVolunteringName,
            otherInfos: updatedOtherInfo,
            buttonName: updatedButtonName,
          },
        };

        const result = await volunteerCollection.findOneAndUpdate(
          filter,
          updateDoc
        );

        if (result) {
          res.status(200).json({ message: "Information updated successfully" });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });
    //-------------------------------------- Media -------------------------------------//
    // Update-media-video route to handle updating video data
    app.put("/update-media-video/:id", async (req, res) => {
      try {
        const id = req.params.id;
        const { videoTitle, videoDescription, videoUrl } = req.body;

        const updatedVideo = {
          videoTitle,
          videoDescription,
        };

        if (videoUrl) {
          updatedVideo.videoUrl = videoUrl;
        }

        const result = await mediaVideoCollection.updateOne(
          { _id: new ObjectId(id) },
          { $set: updatedVideo }
        );

        if (result.modifiedCount > 0) {
          res.json({ message: "Video updated successfully" });
        } else {
          res.status(404).json({ message: "Video not found" });
        }
      } catch (error) {
        console.error("Error updating media video:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });
    // Update-media-image route to handle updating image data
    app.put("/update-media-image/:id", async (req, res) => {
      try {
        const id = req.params.id;
        const { imageTitle, imageDescription, img } = req.body;

        const updatedImage = {
          imageTitle,
          imageDescription,
        };

        if (img) {
          updatedImage.img = img;
        }

        const result = await mediaImageCollection.updateOne(
          { _id: new ObjectId(id) },
          { $set: updatedImage }
        );

        if (result.modifiedCount > 0) {
          res.json({ message: "Image updated successfully" });
        } else {
          res.status(404).json({ message: "Image not found" });
        }
      } catch (error) {
        console.error("Error updating media image:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    //get all video
    app.get("/all-media-video", async (req, res) => {
      const query = {};
      const allVideo = await mediaVideoCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allVideo);
    });
    // post media video
    app.post("/post-media-video", async (req, res) => {
      const addVideo = req.body;
      const result = await mediaVideoCollection.insertOne(addVideo);
      res.send(result);
    });
    //delete media video
    app.delete("/delete-media-video/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await mediaVideoCollection.deleteOne(query);
      res.send(result);
    });

    //delete media image
    app.delete("/delete-media-image/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await mediaImageCollection.deleteOne(query);
      res.send(result);
    });
    //get all image
    app.get("/all-media-image", async (req, res) => {
      const query = {};
      const allBook = await mediaImageCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allBook);
    });
    // post new image
    app.post("/post-media-image", async (req, res) => {
      const addImage = req.body;
      const result = await mediaImageCollection.insertOne(addImage);
      res.send(result);
    });

    // Route to get all media (both videos and images)
    app.get("/all-media", async (req, res) => {
      try {
        const allVideos = await mediaVideoCollection
          .find()
          .sort({ _id: -1 })
          .toArray();
        const allImages = await mediaImageCollection
          .find()
          .sort({ _id: -1 })
          .toArray();

        const allMedia = {
          videos: allVideos,
          images: allImages,
        };

        res.json(allMedia);
      } catch (error) {
        console.error("Error fetching all media:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    //get all trial data
    app.get("/all-try-course", async (req, res) => {
      const query = {};
      const data = await trialData.find(query).toArray();
      res.send(data);
    });

    app.get("/try-data/:videoTitle", async (req, res) => {
      const videoTitle = req.params.videoTitle;
      const query = { videoTitle: videoTitle };
      const courses = await trialData.findOne(query);
      res.send(courses);
    });
    //
    app.get("/get-trial-data/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const courses = await trialData.findOne(query);
      res.send(courses);
    });
    // Route to get media (both videos and images) by isPublish status
    app.get("/get-media/:isPublish", async (req, res) => {
      try {
        const isPublish = req.params.isPublish === "true";
        const query = { isPublish: isPublish };

        const allVideos = await mediaVideoCollection
          .find(query)
          .sort({ _id: -1 })
          .toArray();
        const allImages = await mediaImageCollection
          .find(query)
          .sort({ _id: -1 })
          .toArray();

        const allMedia = {
          videos: allVideos,
          images: allImages,
        };

        res.json(allMedia);
      } catch (error) {
        console.error("Error fetching media by isPublish:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    // Set isPublish to true for media video
    app.put("/make-video-isPublish-true/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await mediaVideoCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    // Set isPublish to true for media image
    app.put("/make-image-isPublish-true/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await mediaImageCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    // Set isPublish to false for media video
    app.put("/make-video-isPublish-false/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await mediaVideoCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    // Set isPublish to false for media image
    app.put("/make-image-isPublish-false/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await mediaImageCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //-------------------------------------- Network -------------------------------------//
    // create new network object
    app.post("/create-new-network", (req, res) => {
      const { newObject } = req.body;
      networkCollection
        .insertOne(newObject)
        .then((result) => {
          // Send a meaningful response back to the client
          res
            .status(201)
            .json({ message: "Network created successfully", result });
        })
        .catch((error) => {
          console.error("Error creating new network:", error);
          res.status(500).json({ message: "Internal server error" });
        });
    });
    // create new expertise object
    app.post("/create-new-expertise", (req, res) => {
      const { newObject } = req.body;
      expertiseCollection
        .insertOne(newObject)
        .then((result) => {
          res
            .status(201)
            .json({ message: "Expertise created successfully", result });
        })
        .catch((error) => {
          console.error("Error creating new expertise:", error);
          res.status(500).json({ message: "Internal server error" });
        });
    });

    // get all networks
    app.get("/all-networks", async (req, res) => {
      const query = {};
      const allNetwork = await networkCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allNetwork);
    });

    // get all expertise
    app.get("/all-expertise", async (req, res) => {
      const query = {};
      const allExpertise = await expertiseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(allExpertise);
    });

    // //delete network
    app.delete("/delete-network/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await networkCollection.deleteOne(query);
      res.send(result);
    });

    //delete expertise
    app.delete("/delete-expertise/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const result = await expertiseCollection.deleteOne(query);
      res.send(result);
    });

    //get network  details by dynamic id
    app.get("/network-detail/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const networks = await networkCollection.findOne(query);
      res.send(networks);
    });

    //get expertise details by dynamic id
    app.get("/expertise-detail/:id", async (req, res) => {
      const id = req.params.id;
      const query = { _id: new ObjectId(id) };
      const expertise = await expertiseCollection.findOne(query);
      res.send(expertise);
    });

    //update network
    app.put("/api/update-network/:id", async (req, res) => {
      const id = req.params.id;
      const { updatedImg, updatedNetworkName } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            networkName: updatedNetworkName,
          },
        };

        const result = await networkCollection.findOneAndUpdate(
          filter,
          updateDoc,
          { returnOriginal: false } // This option ensures that the updated document is returned
        );

        if (result.value) {
          res.status(200).json({
            message: "Information updated successfully",
            result: result.value,
          });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    //get location by isPublish network
    app.get("/get-network/:isPublish", async (req, res) => {
      const isPublish = req.params.isPublish === "true";
      let query = { isPublish: isPublish };
      const result = await networkCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });

    //make publish false for network
    app.put("/make-network-isPublish-false/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await networkCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make publish true for network
    app.put("/make-network-isPublish-true/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await networkCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //get location by isPublish for expertise
    app.get("/get-expertise/:isPublish", async (req, res) => {
      const isPublish = req.params.isPublish === "true";
      let query = { isPublish: isPublish };
      const result = await expertiseCollection
        .find(query)
        .sort({ _id: -1 })
        .toArray();
      res.send(result);
    });

    //make publish false for expertise
    app.put("/make-expertise-isPublish-false/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: {
          isPublish: false,
        },
      };
      const result = await expertiseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });
    //make publish true for expertise
    app.put("/make-expertise-isPublish-true/:id", async (req, res) => {
      const id = req.params.id;
      const filter = { _id: new ObjectId(id) };
      const updateDoc = {
        $set: { isPublish: true },
      };
      const result = await expertiseCollection.findOneAndUpdate(
        filter,
        updateDoc
      );
      res.send(result);
    });

    //update expertise
    app.put("/api/update-expertise/:id", async (req, res) => {
      const id = req.params.id;
      const { updatedImg, updatedExpertiseName } = req.body;

      try {
        const filter = { _id: new ObjectId(id) };
        const updateDoc = {
          $set: {
            img: updatedImg,
            expertiseName: updatedExpertiseName,
          },
        };

        const result = await expertiseCollection.findOneAndUpdate(
          filter,
          updateDoc,
          { returnOriginal: false } // This option ensures that the updated document is returned
        );

        if (result.value) {
          res.status(200).json({
            message: "Information updated successfully",
            result: result.value,
          });
        } else {
          res.status(404).json({ message: "Information is not found" });
        }
      } catch (error) {
        console.error("An error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
      }
    });

    // //react quill

    // app.get("/get-quill-data/:id", async (req, res) => {
    //   const id = req.params.id;
    //   const query = { _id: new ObjectId(id) };
    //   const courses = await trail.findOne(query);
    //   res.send(courses);
    // });
  } finally {
  }
}
run().catch((err) => console.error(err));

app.get("/", (req, res) => {
  res.send("Porject Server is running!!");
});
app.listen(port, () => {
  console.log(`Project server is running on ${port}`);
});
