import React from "react";
import AdminNavbar from "../Navbar/AdminNavbar";
import AdminAbout from "../AboutSection/AdminAbout/AdminAbout";
import { Outlet } from "react-router-dom";

const AboutLayout = () => {
  return (
    <div className="bg-white min-h-screen font-sans">
      <AdminNavbar />
      <div className="flex justify-around">
        <AdminAbout />
        <Outlet />
      </div>
    </div>
  );
};

export default AboutLayout;
