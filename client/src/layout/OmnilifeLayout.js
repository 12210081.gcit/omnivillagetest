import React from "react";
import { Outlet } from "react-router-dom";
import AdminNavbar from "../Navbar/AdminNavbar";
import AdminCourses from "../CoursesSection/AdminCourses/AdminCourses";
const OmnilifeLayout = () => {
  return (
    <div className="bg-white min-h-screen font-sans">
      <AdminNavbar />
      <div className="flex justify-around">
        <AdminCourses />
        <Outlet />
      </div>
    </div>
  );
};

export default OmnilifeLayout;
