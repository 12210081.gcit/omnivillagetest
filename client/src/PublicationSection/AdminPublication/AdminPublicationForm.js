import React, { useState } from "react";
import { toast } from "react-hot-toast";
import { FaPencilAlt } from "react-icons/fa";
import AllPublication from "./AllPublication";

const AdminPublicationForm = () => {
  const imgBbKey = "f375a7d7cbf6443df6506145faff4414";

  // display image
  const [previewImage, setPreviewImage] = useState();
  const check = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      setPreviewImage(e.target.files[0]);
    }
  };

  const handleAddBlog = (event) => {
    event.preventDefault();
    const form = event.target;
    const title = form.title.value;
    const description = form.description.value;
    const link = form.link.value;
    const img = form.img.files[0];

    const formData = new FormData();
    formData.append("image", img);
    const url = `https://api.imgbb.com/1/upload?key=${imgBbKey}`;
    fetch(url, {
      method: "POST",
      body: formData,
    })
      .then((res) => res.json())
      .then((imgData) => {
        if (imgData.success) {
          const addBlog = {
            title,

            img: imgData.data.url,
            link,
            description,
          };
          console.log(addBlog);

          fetch(
            "https://omniverse-server-3222344a8118.herokuapp.com/post-blog",
            {
              method: "POST",
              headers: {
                "content-type": "application/json",
              },
              body: JSON.stringify(addBlog),
            }
          )
            .then((res) => res.json())
            .then((data) => {
              if (data.acknowledged) {
                toast.success(`You have added a blog successfully`);
                setPreviewImage(null);
                form.reset("");
              } else {
                alert("Error");
              }
            })
            .catch((err) => alert(err));
        }
      });
  };
  return (
    <div className="bg-white ">
      <div className="bg-gray-100">
        <div className="flex justify-center items-center  py-16 min-h-screen px-3  ">
          <div className="flex flex-col items-center space-y-1 shadow-lg  w-[800px] px-3 md:p-10 md:space-y-3 border-2 rounded-md bg-white ">
            <h1 className="text-xl md:text-3xl text-center text-gray-800 font-semibold mb-10  border-b w-full pb-2 mt-3">
              Post new article
              <FaPencilAlt className="inline-block text-teal-accent-400 mb-2" />
            </h1>

            <div
              onChange={previewImage}
              className="hover:scale-110 duration-300"
            >
              {previewImage ? (
                <img
                  className="h-60 w-60"
                  src={URL.createObjectURL(previewImage)}
                  alt=""
                />
              ) : (
                <img
                  className="h-60 w-60 mb-5"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgVFRYZGBgaGRgaGBgYGhgYGBoYGBgZGRgYGRgcIS4lHB4rIRgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHzQrISs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAEBQADAQIGB//EAD4QAAIBAwIEAwUFBgYBBQAAAAECAAMEEQUhEjFBUQZhcRMiMoGRFEKhscEHFVJigtEjQ3LC4fCSFiQzsvH/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAlEQACAgICAQQDAQEAAAAAAAAAAQIRAzESIUETUWFxBCIykRT/2gAMAwEAAhEDEQA/AOsNUCRbmBMZdQWa2YB9KqTDqTQCmBCFaAw4SFZQlSXLUgMm83V5A0hEQyxKktDwUpIGIiGGZkgy1JuKkLAukmqvNoASSSSAEkkmMwAzJMcUmYWBmSYzM5hYEkmOKYLjvFY6NpJU1Ud5Wbkd4+SCmESStKgMsBhYiSSSQAxJMzEAMSTMxGBiSZkgBxYlqShDCEgZlq1JelWDYmBHYB6VJerxWry5asBjRXlivF6VpaKsQw4PNXqCL6t2B1lK1+LlJlJIuMXLQc1eboSYPSo9YYgmbdmqikXUISIMrASPcARqVEuNhJMwWi970DrBK+pqOsHMFAa1LgCAXOqKvWc/fav2MRXN2xPOZyyG8MF7OvbWR3my6wO84f7T5yp7095n6rNfQR3za0o6yp9fXvPPXvj3mhuvOHqsfoRO7qeJB3gtTxLOKa585W9z5yXkkWsMEdmPEmTuYUt/xjIM8+9vC7TUGU84Kb8iljXg7211JlO+4j211BW6zgbbUlbnsYctcjdTNY5KOeWFM75HBlk4yx17hOH+s6O11BXGxE3jNSOeWOUdjCSYRszMogxiYxNpIAayTaSAHCIYSjQNZejQMwnMmJoplimMZCJBNgJuKLHkpPyMQ6Kw0Gur7gELe3fHw49cD84ovtJd/wDMRfIljt8hIlOkaQx8mAHUWdsDlHmn1YsttHC/5iH5MP0jm2tCORU+hnOm27Z2NRiqQ1pVRLhUi4Iw6TPtMS7M2g96kXXIY8jKnvJPtgg5AosXV6VToYI9q55mN3uhFte+GdplKjeN+xR+6y3UzV9Bc8jGdpd940o3ayoxTFKconJv4cfGczmtURqRw31npd7qyKJ534ouhUziDjG6QRnKrYie9mi38CdJVwx8ER6smxmbzMqe6MtsKQPOHVrVcSLinRqoyasV07uG0biD/ZRmH0rYYibiVGMixLjzjG21EjY8olqJgzKPF9FfZ061Q4yJWmovRYEMcdootbsqYxrAOscURJ1vR3+g68tRRvvOkR8zwq3vXoOCCcZnpHh/xKlRQC2DOmGTwzjy4q/aOjsJJTSrgjnLGqCamBnEkq+0iSAWcuUpj7h+pm6Mg+4frMHUU/hH0zJ+9VHLb02mDl8nQor2CPtCD7g/OVtdr0TJ9JX++R3MHr68oHX9InL5KWO/AUb9+iEein+0Eq6s3UxZV8RDuR6Ej69cys6wlTZ/k33h8+3kZHK9M1UOO0X3OpMesW1LsnrAL+uUbDbg7q3Rh3EHS6BmTbbOiMUl0MEvSp33jaw1XfoJz4wZYhxHG0RJKR31tehuZ+ed5dUocfUHzGx+YnEUbtl6wylrDCa8l5Of02n0OLnT3B23/A/SBPkbHb1hNtr+dm38jDzXpuN8eQP6HnFSemUpSj/SOerMYvfOZ0d1pWclGHoeX/kP1iG9oOm7oQP4uY+omcos3jOLRBVImwvSBzldtaVKnwLt3O0bW3hF3+N8DsP7yoxk9GcskFtnNXNR6rcCAsT26escWPgtnXLk5PadrpXh+nRHurv1PU/ONwqrN4Yku3s5smZy6XSPP0/Z9SzlgT6mMKHg2gg2QfSdTXu0XmREuoeI6aZywmqiczl7sR6l4WogEhQD5bTz/WbZqbEKTidZqnjumcqoLek5S6vxVbiOwilji9lxzTjoULUfMMocZhaIg7QgVF6SPRiX/wBMwT7Ox84RR0tjuYTSrKJs+qKOUfpRE/yJsV3dApJbajw7HlNb2uanIQJLFycDeQ8Tv9TWOdcf2LtSug3KV6fXqZHDmONO8Ns27TqbLRkpjlvNFhtdmUvyqfQf4e1KoEHGd43e/ZuUV06BPKFilwDLTaMUlRzyk5Oy/ibvJE76icnA26SS+LI5IBDzV3it9RA6zQ6hmeXZ7XEaM8GrHIgYvh3lqVgZJSF12pEHo1yCM745RncqDFVWnKiTLsZvVFZOBzv0I5qe4nN1Kj03KNsR+PYjyjOi5E11O2FRcj4xyP6GXSZCk4lVtqMbULkGcarlTjHLnGNteYicaGpWdWlSZMV291nrC0qyGWkE8RHKXU73g364AED45W28TvwUknseUNWxyJz3zH1neh9mGe//AHrOGVcbwm31Bk5GVGVbInBP+T06xooPhxD2ukUbkTgNJ1tmbDtsPxPaPdStFuaZRaxR+jp0P6idMJJo4ckGmEar4ro0h7zgeWd5xup/tEG4RT6nacn4h8L3NsxaopdDyqrll/q6r84nS2Zuk1XwYv5Y31DxhXqbA49IkqvVqHLsT844tNKyO0KFqF2AzK4yZPKKOdSzPMwlKTdI7pWWTvDk04LuYcA5nP07Zz3hKUOHzjoUOLYD8IVR0vrgx8BchAtqzd4XR0rqTH60UQ4I37Q200sucsCB25R8V5FybEFtpZbYDadBYaKq8wI4pWSoNhBa96GyicxzIGwjXwKvcxWZU2Xc+UlC2LHJmtsgHMM7ekF1XxN9nIX2Lsx5BQCYfQuvI5d0prknH4RUiPctuMJnbuZVY2D3LCtXUoo3VM/i3n5TqaNEAAKNodL7Glf0B09PQADA2kjX2UxFyZXFHlereGCQXtm4uvs2O/8AQx5+h+s5hLoqxRwVYHBVhgj1Bnc2t5wzN/Z0bkf4ijiHJxsw+fUeRnDUWelckcatdZclzjfMOufDRp7rl18ufzXr8oodE5BD8yfykSSRcZN+Bkl1mRgDFi5HTlCaVeSi38m9SnKHrEQovkQG5SUjNoFuWGeIDeDGvLnE1sNPavVWkhAZjgcRx57eflLqxKXFDHQ7M1n4FJU9xuPmJ0Nx4cuae6hag8jwt9D/AHnW+F/DKWyb+85+Jv8AvSOqqgzWOFV2c0s7vo8lq1Shw6sh7MCPzliVMz0i4tkIwwBHYgEfjEl/4eoPumabd0A4fmnL6Yky/HfguH5av9kcsHkIk1HTqtE+9hl6Mp/NTuIEt1MHCUdo6ozjLtMOQ8O4MZ2mqsmN5z/2mVVrraJNrQ3GL2egW/idccGx4tmB+HB58XTERVbWmjngXKE5TbJ4TynIUXd3VEBZmICqOZJnrGmaO9NFDlSQAD69Z1YJSbd6OH8qEElx2c/Q04v90gemIamjqOaEx/XJRSSyDHSLrTUatQkMhRO+DkjvOq2zjpIXtQpZ4VA4vyl9HRTniZxjtHVC1pDcUmJ6nh/vK7/V6FuB7VCudlBGST2GIW/AUvIPTsUA+ID6QZqbs4Slg92PITa59nclWfKIN+EEhm9cRrQWkm1JHI8lP5mGg2VW+kFDxOykwqvVRB77qMRTrHiNLYqHpOWfZVwCT8sxammNcuK90BTQfBTJAA86h5Z8oVfbHddIJ+01a74pNiiObY3b/T5ece2VmEGAAO5MXnVbamhPtsqo39krOABzyUU4nIeIvFaXIFCzR2NT3Q54w7HsgJyPUxMEvJ0PifxMbfFOhwvVJA4QOWfTr5TPh/Rapf7Tdtl2Hup0Qdh5zHhPwkLZRVrkPWI26hc9FzzPdp1dOmWOT9OghdaCr2YppnlsIWiATKJibSSyZ8pJJIAeTMJqtcrNy0oqTzmz10r2FLqhHOWp7Cqcsu/UjY/8xFXWU0axU5EE/cUorwd9a+FqFQZRz5g4yPlA9Y/Z+3AXt2y434DgB+4B6N67Hy5xbpOulWHCSGHXOJ3ul+I0f3KvuP3Pwn0PSbxUGcs3ki92eMioVJVgVYEhlYEEEbEEHcGYdszuv2iUrWp/iI4FwMA8G4degfHIjo3yO2Mede0IODse0mUHFlwyKSJUEHcYxgkEbgjYgjcEEcjCScyqsNokUzuvC3jV3K0bg5c7I/LjPRXHRvPr68+0U1DuMATwoDrO10nWa9wiUEbhq5x7RnwCoGc8PVvz5zoxzvpnJmx1+0TualVkBZyuB3iddYrOT7OgeHlxnYHzA6w/TNACgNXqNWf+b4R6KNp0FEqo2A/SdFpHNTZzVG0UkPVps7ea7D0mup2ts6k1LYgAbsBwkf1LvOjq34J4V95v4V6f6j0mBb8W74P8v3R/eJ97GrX8s8zfRKNU/wDtnqD/AFqCnybY/nLU8DO3x3CAdeFHc/mJ6gqoPuj0AE249jgDPTtnzmTxxb0axyzS2cZpuiWlkONnbjIx7R/dPmF7CX17pK6FbTjdztx8RCL5ljsfQTN94Ue4qcdzc8YHw01TgRflxZMZ0tFZVCrWZVGwCIigD6Gax4x0ZScpO2KtL0KrT96ofaP3d9v6Vj9Wq4+Kmv1P6RPrK0bZOO4uaw/hUMgd8dEVVyfXkO85C00u5vn4+OtQtT8PHUZndf5RsDn+IjHbMH32JddHR6n4jufbfZ7XgrOPjZQ3DT/1EQmxsqzP7StTatU6FuFET/QpOfmd4bplrStkCUUCIOvNmPUk82bz/KXXmqJSQvVcIg6nmdvhAG5PkIXWgq9m7/aD9yknzLfgAPznIX/iK5et7C0dajD4mVMIu+5LEkYHf6ZmzX1zqRKUeKhaA4eofjqY5qoHM9MDYdSfhjylY2ltQakzpQRgQzM6I7ZGCSzdTy/LELQ6Ylp3FpbMal3dLUuPvHd2TP3URQSg8yIJW8eWCHKW9Wqw5O6p+Bdiw+ktY6HbD4adVuYU8dYsemA2VH4Cc/ZJ+8LoKlqi0Uzlaf8AgrTXI953Rcux6L16YGTE5NjUUi3UfFVzqLJb29MorZ/wlbJIBwWqPgAINumPU4nb+GfC9KxUH/5Lhxu3bqVQH4EB68z17Q2y063sl4Lemod+g+JsdXbngRjaWpHvOeJ2+Jv0A6DyisZdSpEnLbn8B5CFqIKwBbAJ4h26ep/SFKNt4AjMhmYHf3iopJMBt0X+1Ek83vPF2HbB2ztJKonl8DC/8Iuu9Gqrj+F/cb6jIP4Tnbmm6MUdSrDoevmDyI9I6panUbrKtT1CmExXZfJTu3yHOcbxxejsjnkt9iKuMxfUXEtq3SbsjYXoHPvfLvKxXVpm4NbNo5FLRpbAjePLbUej7jz5/WJVfB8prcVwo5xp1obVvs6ZKSVGHAQWOwDbb9AT1i7WPDNwxDt7FOSj3yCxPIZI3MUaRcV3qj2ABdfe9/PAOxbG+PSO/wD0zXuaqve3HGowQiBh6quwCjzAzOnF+0akjiy1CdxZylZHRyjqVdTgqeY/uPOaMczvfE2iNVVEt6CKKeAtRnYVMdVI4TkepO/acJdWr0nNOovCw6dx0IPUHvM5QcX8G0MikvkoeE6fdNTYMp/6JVw5mEWZmh6HbeI2eizK9Zqw2SmgpkMTsCOJc4HXnDNNtblxxXlzwKf8pCiH+txuPRT855vRuWRgQcEHpKb3TGd2qK1FFc8QDVaSYyAT7pbI3ztjrOjHkb6OXLiS7R7Qmp21FeH21BAOhqIPzbJMrfxTaDnc0v6XDf8A1zPG7Xw5UckLVt2I3IWqGwPPgBjWl4WVQPaOxbrwYVR5ZYHPrtNLbMekeiv43sl/z8+iOf8AbB3/AGhWg5M7eiH/AHYnDXWhWyLn2jO3ZXXGfp+MV/Y6YGWIAHMlvyA3JiGeit+0m36JUPyQf7osv/2ku4KW1Io5243KsR5qg2+bHHlOPs7eg7kuhRNgp4ypPmRg5+RA9Z1mj/YqQwtMOepFNqjfNiCYxNoU6fd0/aGrcI93WzndvcB6cwS+OmwUdBtmdQNdvam1OyYDuyuR9SFEe0/Egp08pbVAqjJIRU+fvETm7v8AaBVqk0rSgxc8iSGI8+Hl8ycR2wpFVW/1IsyJQYMuAeCmpAOMgcbEqNiNszfSPCFxXqCtqLNwjlTLhmbfPCSp4UTyXc+XMtvCfh96DGvcVWeq54igduBSRuWGQHfzIwOg6zo6r0Ru759W2i+wKbizt8AO5RFAC00coigDGOFCMjyO0SX/AIj021VvYrSap/AiqWLfzMBt85T4m8Y21BClAI7nYYwQPMkRF4W8EvdP9puvdRjxcGOEt5t2HlEMD0vRLjVq5uKp4KYPx8OMgfcQdcfxH8enp9tYLbU1o2yAeewAPV3PNjGttbqihEUKoGAAMAASm8QMCOWOZ5YHfMAKbW1VNy3E5+JjzJ/QeUw9yztwU+Y+N+YXyA6t+A/CB2zvWPDSJSiNjU5PU78H8Kfzcz0wNy6t7dUUKowBHoNkoUQgwPmTuSe5PUy2TEquKwUZMQ9Gl1cBASTPMfFviAuxpodupEM8XeIjuiHc9pxJcKC7H/mXohuyxaA9ZiJqmo1MnGw6TMnkh8WMrzxPUb3aY4F783Pz5D5RI1V3ORxMx67s0H3/AIh9ZsGYdfxmVM26Ru5cHdzn1z+Myty45uQPQGDNUbv+Ui0y3OOgsNGpsNmwfMcpKt6zcvzhFkiLjKgnz3jqkts/x0kz3AwfwxJUIsbyyQisdZuKIIpsqg7kcCHJ9SMmMV8a3o++v/iP0Mf2+m2Z/wAlD/5f3l9xZ2FNeJ6SAdM5JPkBneaKPWzFyTfaOcTxle1GCBgGYgZVOJ/6Qc5Py+ky2k+yZql9WPEwYimp467nGFZi3wjl8XpM3GuqpP2SilEcvaBR7QjyP3YmZixLMCSTkk5JJ7k9YFL/AAtt6pbYKf0mXcg4GCfLeU+0x5TYBz8CkeYEjgi/VZmshABZsMfhUDJP9p2tCnQNJS1pR9oVHEeBeHP8RB6ntOStrV1PEqkt/EecPWhcHv8AWaRilpGcpuW2NKWgswLCp7NSfhUhF+SKABAbvSlDEB2YDmWfP0gVytVSFJJJ6Ay6jodepgn3R5k5joi/kEr0kB4EHG3fJwPn3hNnpiD3nYE9ug/vGtPw2VGOLA8oTbaAhOCx+spRByBaBoLzx9N41t/EtvRGcH5CAataW9IBRlmPIDJML0rRabYZkJ9f+Ydk9bCvtNzqPuUwaND7zn4mHYCPG8Om3phLIKrsffqOCxPcnuYdaMEUKq4A6CVal4jFFSW/ARUOxJW8JXTjNa8qeiAIJyNz4Ud6pppU4/NySY5vfGFa7PsrZSCdix6DvOv8HeGfYLxuSztuzHv5RdFdiTwp+zxabh65DkbquPdB746mekJTCjA2Am2MQO4ucbDnFsZbVrgbRTVtnrvwsOGiD8PWoe7/AMv8v1httQJPE0PVY9BslNAoAAwBN8SASutUAEkZivWCicJ4s8Q8IKqdzCvE+vBFIB3nm9So1Ryx3miVEN2aM5JLsfWKb65LnA5CNtUThSJadOTL2Kj7mnBJC/ZzMVDsVGnI1djsPyjGnYFt3PyENWyp4xw7xKI3JCNKRPOEMnRR8zzjZLFOo2mle3pry3PaHEOQsS3Pf8ZY9UDZdz5Q620vi3fYdoySxprsFEaiJyALF3A95x6doZWZGxnGR1O5hVOzTtM3SUUHLJlqNEN2LzwDtJVvFxwooJ8hNbew424m2XtHltSpIMYEFYNpCC3txnifcnpjYRpTrYGFU49If7SnnpN31Gmg2AJjqiW7AGvVTdgRMJqb1Bikh9eQlXD7d8sMLnlOjsiiABViDpCiw0t1bjccTR0lOr2AhJuyBnhxFtz4mRDg849BsuubaoBlmnMNd13conLOC0ci4q3OwyqHr1MfafoSIBtvEGhbo+iop46h437mdPSrU16TUWQA5xHrddUU+9iADPUvEFJFO4E4apcVdQq8CArTzu3cQfT/AA/UuqnExPBnrneeraDoaUUAVQIiinw14Zp26jhXfqep+c6XkJFIEytPi58pLKSKWZm2X6zZLVV3O5hRwolXOFjowolkwBNXfEQzFR8Tl/EOsBFO8J1rVQineeW61qTVXIztLSrszbvoHv7tqzkwu0oACD2lLEO4tpSExZrZyAIuo0odqJywExb05NWx3SMewkjFaUkqieQMNGfGQ0CvKLptxZMYPqFRhhVMqoo2eJxkwaXgab8g9np9WpzOBDE0Ngee8Np3LjkssFdz0gooHJg9PSHP3pVd2DIPiJh5rVOggzWlVzluUKFYttLWq554Ed0dBXGWOT5mXW9u42Etqh1GSYKNCbbNE0pe8L/dlNVycRFV1VweFcmXLSq1BuSIWFe4HfW3G/ChwPKONM0ZFALbnzmtppZTuTGlK0aFBfgsS1pjtLkNNe0W6ieBcico93Vd+EHAg3QJWdfquqIF4V3PYRNY6C1R+NhDtF05dmc5PnOstmRRgQ+xr4BLHTuAYEJrHgGYW12iic1r+vKoIXcnkIWFews1nxOUPCMkwXSdNq3Th6uQmche/rCtF0E1n9pUHPfHaehadp6oAAIvsa+DOlacqKABjEa8pSWxLKaE7yWWlRZTTqYQWwJUoxNScxFGSczYCagSM+IhGXfESarqIRTvLtRvgoO8828Sa2WJUGUkS34BPEOsF2Kg7RPQp9TK0XJyYYgxKJ0EJtMs8q4ppUeAUDVBlodbUpRb08mOLSjHFCkzZaEkZLRklkWCJSQDlNwF7SSRiLF4e03Ur2kkklFoqL2mTcL2kkgBPtqjpFOqaqCMASSRMaBNPVSeIiPEulUbCZkgJmp1RR/+QevrmOUkkGNCe4uXrHGcCYS2CDJmJIAzT9/cOyzQ+Jn6CSSQ2y+KKn1ys+w/OPNA0UuweocmSSNCfSPRdOtQoG0YySRMpFlvQycmGnAEkkllIDqVhmV+2kkgBGuIBeX+BJJBCZwniLWjuBOKZyxyZJJbJReksDSSQQmbFpUp4jJJDyHga2lGOrWjJJNDNjNaMkkkkZ//2Q=="
                  alt=""
                />
              )}
            </div>

            <form
              onSubmit={handleAddBlog}
              className="flex flex-col items-start space-y-3 border-2 p-2 rounded-lg hover:shadow-gray-700  w-full"
            >
              <label
                required
                htmlFor="addPublication"
                className="px-4 py-3 bg-gray-800 text-white text-xl font-semibold rounded-lg cursor-pointer"
              >
                Upload an image
              </label>
              <input
                className=" px-3 py-2 rounded-lg shadow-sm border  border-none w-full 
                            focus:outline-none  bg-white text-gray-900 hidden"
                type="file"
                name="img"
                id="addPublication"
                onChange={check}
                accept="image/*"
                required
              />

              <textarea
                name="title"
                placeholder="Title of Blog"
                className="w-full h-20 pt-3 px-2 text-lg md:text-xl border bg-white border-gray-200 rounded-md
                         outline-none text-gray-900 "
                required
                maxLength={100}
              ></textarea>

              <input
                name="link"
                placeholder="Link of Blog"
                className="w-full h-16 px-2 
                        text-lg md:text-xl border border-gray-200 rounded-md outline-none text-gray-900 bg-white"
                required
              />

              <textarea
                name="description"
                placeholder="Some description of Blog"
                className="w-full h-28 pt-3 px-2 text-lg md:text-xl border bg-white border-gray-200 rounded-md
                         outline-none text-gray-900 "
              ></textarea>
              {/* button */}
              <button class="relative inline-block text-lg group" type="submit">
                <span class="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
                  <span class="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
                  <span class="absolute left-0 w-48 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
                  <span class="relative">Post now</span>
                </span>
                <span
                  class="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
                  data-rounded="rounded-lg"
                ></span>
              </button>
            </form>
          </div>
        </div>
      </div>
      <AllPublication />
    </div>
  );
};

export default AdminPublicationForm;
