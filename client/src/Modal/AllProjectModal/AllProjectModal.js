import React, { useEffect, useState } from "react";

const AllProjectModal = () => {
  const [projects, setProjects] = useState([]);
  useEffect(() => {
    fetch("https://omniverse-server-3222344a8118.herokuapp.com/all-projects")
      .then((res) => res.json())
      .then((data) => {
        setProjects(data);
      });
  }, [projects]);

  return (
    <div className="bg-white">
      <input
        type="checkbox"
        id="my-project-modal"
        className="modal-toggle bg-white"
      />
      <div className="modal">
        <div className=" modal-box w-11/12 max-w-[1200px] bg-white  relative">
          <label
            htmlFor="my-project-modal"
            className="btn btn-sm btn-circle absolute right-2 top-2"
          >
            ✕
          </label>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 lg:gap-8 xl:gap-10 max-h-[100vh] overflow-y-auto scrollbar-hide">
            {projects.map((project) => (
              <a href={project.projectLink} target="blank">
                <div className=" flex flex-col bg-white justify-between items-center  h-[400px] w-full md:w-[310px] xl:w-[370px]  md:h-[470px] my-4  shadow-xl  rounded-2xl pb-5">
                  <img
                    src={project.img}
                    alt="project"
                    className="h-[300px]  md:h-[350px] rounded-t-xl"
                  />
                  <h1 className="text-base md:text-xl px-3 pt-1 font-normal text-gray-800">
                    {project.description}
                  </h1>
                </div>
              </a>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AllProjectModal;
