import React from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";

const AllCourseDetailsModal = ({ selectedCourse, course, handleClose }) => {
  return (
    <div
      id={course._id}
      className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50"
    >
      <div className="relative bg-white w-11/12">
        <div className="flex justify-between items-start">
          <img
            className="xl:w-4/12 xl:h-[500px] p-4 rounded-2xl"
            src={selectedCourse.img}
            alt="course"
          />
          <div className="px-7 py-5 space-y-3 pt-7 w-8/12">
            <AiOutlineCloseCircle
              className="text-3xl absolute right-3 top-3 text-gray-800"
              onClick={handleClose}
            />
            <h2 className="text-2xl text-gray-800">
              <span className="font-bold">Course Name :</span>{" "}
              {selectedCourse.courseName}
            </h2>
            <p className="text-2xl text-gray-800">
              <span className="font-bold">Course Fee :</span>{" "}
              {selectedCourse.courseFee} INR
            </p>
            <p className="text-2xl text-gray-800 leading-10">
              <span className="font-bold">About Course :</span>{" "}
              {selectedCourse.description}
            </p>
            {/* button */}
            <div className=" absolute bottom-5 right-3 bg-white">
              <a href="/rayzorpay">
                <button
                  class="relative inline-block text-lg group"
                  type="submit"
                >
                  <span class="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
                    <span class="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
                    <span class="absolute left-0 w-56 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
                    <span class="relative">Register</span>
                  </span>
                  <span
                    class="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
                    data-rounded="rounded-lg"
                  ></span>
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AllCourseDetailsModal;
