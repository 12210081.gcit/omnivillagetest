import React, { useState } from "react";
import { toast } from "react-hot-toast";
import { FaPencilAlt } from "react-icons/fa";
import AdminAllImages from "./AdminAllImages";
import AdminAllVideo from "./AdminAllVideo";

const AdminMediaForm = () => {
  const imgBbKey = "f375a7d7cbf6443df6506145faff4414";

  // display image
  const [previewImage, setPreviewImage] = useState();
  const check = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      setPreviewImage(e.target.files[0]);
    }
  };

  // add image
  const handleAddImage = (event) => {
    event.preventDefault();
    const form = event.target;
    const imageTitle = form.imageTitle.value;
    const imageDescription = form.imageDescription.value; // New: Description field for images
    const img = form.img.files[0];

    const formData = new FormData();
    formData.append("image", img);
    const url = `https://api.imgbb.com/1/upload?key=${imgBbKey}`;
    fetch(url, {
      method: "POST",
      body: formData,
    })
      .then((res) => res.json())
      .then((imgData) => {
        if (imgData.success) {
          const addImage = {
            imageTitle,
            imageDescription, // New: Add description to the object
            img: imgData.data.url,
            isPublish: true,
          };
          console.log(addImage);

          fetch("https://omnivillagetest-xxd5.vercel.app/post-media-image", {
            method: "POST",
            headers: {
              "content-type": "application/json",
            },
            body: JSON.stringify(addImage),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data.acknowledged) {
                toast.success(`You have added an image successfully`);
                setPreviewImage(null);
                form.reset("");
              } else {
                alert("Error");
              }
            })
            .catch((err) => alert(err));
        }
      });
  };

  // add video
  const handleAddVideo = (event) => {
    event.preventDefault();
    const form = event.target;
    const videoTitle = form.videoTitle.value;
    const videoDescription = form.videoDescription.value; // New: Description field for videos
    const videoLink = form.videoLink.value;

    const addVideo = {
      videoTitle,
      videoDescription, // New: Add description to the object
      videoLink,
      isPublish: true,
    };
    console.log(addVideo);

    fetch("https://omnivillagetest-xxd5.vercel.app/post-media-video", {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(addVideo),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.acknowledged) {
          toast.success(`You have added a video successfully`);
          setPreviewImage(null);
          form.reset("");
        } else {
          alert("Error");
        }
      })
      .catch((err) => alert(err));
  };

  return (
    <div className="bg-white ">
      <div className="bg-gray-100">
        <div className="flex justify-center items-center  pt-16 pb-9 min-h-screen px-3  ">
          <div className="flex flex-col items-center space-y-1 shadow-lg  w-[800px] px-3 md:p-10 md:space-y-3 border-2 rounded-md bg-white ">
            <h1 className="text-xl md:text-3xl text-center text-gray-800 font-semibold mb-10  border-b w-full pb-2 mt-3">
              Add an Image{" "}
              <FaPencilAlt className="inline-block text-teal-accent-400 mb-2" />
            </h1>

            <div
              onChange={previewImage}
              className="hover:scale-110 duration-300"
            >
              {previewImage ? (
                <img
                  className="h-60 w-60"
                  src={URL.createObjectURL(previewImage)}
                  alt=""
                />
              ) : (
                <img
                  className="h-60 w-60 mb-5"
                  src="https://img.freepik.com/free-vector/female-journalist-with-microphone-interviewing-man_74855-7350.jpg?t=st=1714650060~exp=1714653660~hmac=bb8d86fccd87a3887752b4e96340568a2cba17a3cbd5f321906b55d99c030929&w=1380"
                  alt=""
                />
              )}
            </div>

            <form
              onSubmit={handleAddImage}
              className="flex flex-col items-start space-y-3 border-2 p-2 rounded-lg hover:shadow-gray-700  w-full"
            >
              <label
                required
                htmlFor="addImage"
                className="px-4 py-3 bg-gray-800 text-white text-xl font-semibold rounded-lg cursor-pointer"
              >
                Upload an image
              </label>
              <input
                className=" px-3 py-2 rounded-lg shadow-sm border  border-none w-full 
                            focus:outline-none  bg-white text-gray-900 hidden"
                type="file"
                name="img"
                id="addImage"
                onChange={check}
                accept="image/*"
                required
              />

              <textarea
                name="imageTitle"
                placeholder="Title of Image"
                className="w-full h-20 pt-3 px-2 text-lg md:text-xl border bg-white border-gray-200 rounded-md
                         outline-none text-gray-900 "
                required
              ></textarea>

              {/* New: Description field */}
              <textarea
                name="imageDescription"
                placeholder="Description of Image"
                className="w-full h-20 pt-3 px-2 text-lg md:text-xl border bg-white border-gray-200 rounded-md
                         outline-none text-gray-900 "
                required
              ></textarea>

              {/* button */}
              <button class="relative inline-block text-lg group" type="submit">
                <span class="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
                  <span class="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
                  <span class="absolute left-0 w-48 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
                  <span class="relative">Add now</span>
                </span>
                <span
                  class="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
                  data-rounded="rounded-lg"
                ></span>
              </button>
            </form>
          </div>
        </div>
      </div>
      {/* video form */}
      <div className="bg-gray-100">
        <div className="flex justify-center items-center  px-3  ">
          <div className="flex flex-col items-center space-y-1 shadow-lg  w-[800px] px-3 md:p-10 md:space-y-3 border-2 rounded-md bg-white ">
            <h1 className="text-xl md:text-3xl text-center text-gray-800 font-semibold mb-10  border-b w-full pb-2 mt-3">
              Add a video{" "}
              <FaPencilAlt className="inline-block text-teal-accent-400 mb-2" />
            </h1>

            <form
              onSubmit={handleAddVideo}
              className="flex flex-col items-start space-y-3 border-2 p-2 rounded-lg hover:shadow-gray-700  w-full"
            >
              <textarea
                name="videoTitle"
                placeholder="Title of video"
                className="w-full h-20 pt-3 px-2 text-lg md:text-xl border bg-white border-gray-200 rounded-md
                         outline-none text-gray-900 "
                required
              ></textarea>

              {/* New: Description field */}
              <textarea
                name="videoDescription"
                placeholder="Description of Video"
                className="w-full h-20 pt-3 px-2 text-lg md:text-xl border bg-white border-gray-200 rounded-md
                         outline-none text-gray-900 "
                required
              ></textarea>

              <input
                name="videoLink"
                placeholder="Link of video"
                className="w-full h-16 px-2 
                        text-lg md:text-xl border border-gray-200 rounded-md outline-none text-gray-900 bg-white"
                required
              />

              {/* button */}
              <button class="relative inline-block text-lg group" type="submit">
                <span class="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
                  <span class="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
                  <span class="absolute left-0 w-48 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
                  <span class="relative">Add now</span>
                </span>
                <span
                  class="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
                  data-rounded="rounded-lg"
                ></span>
              </button>
            </form>
          </div>
        </div>
        <AdminAllImages />
        <AdminAllVideo />
      </div>
    </div>
  );
};

export default AdminMediaForm;
