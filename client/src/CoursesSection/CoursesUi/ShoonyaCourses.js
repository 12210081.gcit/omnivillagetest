import React, { useEffect } from "react";
import { Link, useLoaderData } from "react-router-dom";
import Footer from "../../HomePage/Footer";
import Navbar from "../../Navbar/Navbar";
import { MdOutlineKeyboardBackspace } from "react-icons/md";

const ShoonyaCourse = () => {
  const course = useLoaderData();

  const {
    courseName,
    img,
    courseTitle,
    description,
    note,
    otherInfos,
    structure,
    fee,
    participate,
    resource,
    research,
    researchLink,
    button,
    structureText,
    batchStart,
  } = course;

  //razorpay button 1
  useEffect(() => {
    const loadPaymentButtonScript = () => {
      const form = document.getElementById("form");
      const script = document.createElement("script");
      script.src = "https://checkout.razorpay.com/v1/payment-button.js";
      script.async = true;
      script.dataset.payment_button_id = button;

      if (form) {
        form.appendChild(script);
      }
    };
    loadPaymentButtonScript();

    return () => {
      const script = document.querySelector(
        'script[src="https://checkout.razorpay.com/v1/payment-button.js"]'
      );
      if (script && script.parentNode) {
        script.parentNode.removeChild(script);
      }
    };
  }, []);
  //razorpay button 2
  useEffect(() => {
    const loadPaymentButtonScript = () => {
      const form = document.getElementById("payment");
      const script = document.createElement("script");
      script.src = "https://checkout.razorpay.com/v1/payment-button.js";
      script.async = true;
      script.dataset.payment_button_id = button;

      if (form) {
        form.appendChild(script);
      }
    };
    loadPaymentButtonScript();

    return () => {
      const script = document.querySelector(
        'script[src="https://checkout.razorpay.com/v1/payment-button.js"]'
      );
      if (script && script.parentNode) {
        script.parentNode.removeChild(script);
      }
    };
  }, []);
  return (
    <div className="bg-white font-sans relative">
      <Navbar />
      <div className=" py-3 md:py-5 px-5 md:px-10 lg:px-20 4xl:px-28">
        <Link to="/participate/omni-life">
          <button className="relative inline-block text-lg group mt-2 xl:mt-3">
            <span className="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
              <span className="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
              <span className="absolute left-0 w-56 h-48 -ml-0 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
              <span className="relative">
                <MdOutlineKeyboardBackspace className="text-2xl" />
              </span>
            </span>
            <span
              className="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
              data-rounded="rounded-lg"
            ></span>
          </button>
        </Link>
        <div className="pb-40  xl:pb-40">
          {/* top container */}
          <div className="flex flex-col lg:flex-row justify-between items-center py-6">
            <div>
              <img
                className=" md:h-96 lg:h-[420px] 2xl:h-[450px] 3xl:h-[500px] 5xl:h-[520px] mb-7 lg:mb-0"
                src={img}
                alt="CourseImage"
              />
            </div>
            <div className="lg:w-3/5 xl:w-4/6 6xl:w-9/12 lg:pl-5 flex flex-col items-center lg:items-start">
              <h1
                id="title"
                className="font-semibold capitalize text-3xl lg:text-5xl text-center lg:text-start text-gray-800 pb-3"
              >
                {courseName}
              </h1>
              <p className="font-normal text-lg lg:text-3xl text-center lg:text-start text-gray-800 pb-4 lg:pb-8">
                {courseTitle}
              </p>
              {/* others */}
              <div className="space-y-6">
                {otherInfos.map((other) => (
                  <p className="font-normal text-lg lg:text-3xl text-center lg:text-start text-gray-800">
                    {other}
                  </p>
                ))}
              </div>
              <div className="">
                <form className="pt-2" id="form"></form>
              </div>
            </div>
          </div>
          {/* description */}
          <div className="pt-10  lg:pt-10 lg:px-24 2xl:px-28 6xl:px-32 space-y-6">
            {description.map((des) => (
              <p className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                {des}
              </p>
            ))}
          </div>
          {/* Structure teach and participate */}
          <div className="pt-10 lg:pt-10 lg:px-24 2xl:px-28 6xl:px-32 space-y-6">
            <p className="text-[19px] md:text-[23px] lg:text-[19px] xl:text-[21px] 6xl:text-[31px] font-semibold text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px] pb-2">
              {structureText}
            </p>
            {structure.map((stru) => (
              <p className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                {stru}
              </p>
            ))}
            {note === "No Note" || note === "no note" ? (
              ""
            ) : (
              <p className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                <span className="font-semibold pr-3">Note:</span>

                {note}
              </p>
            )}
          </div>
          {/* fee and cost */}
          <div className="pt-10 lg:pt-10 lg:px-24 2xl:px-28 6xl:px-32 space-y-6">
            <p className="text-[19px] md:text-[23px] lg:text-[19px] xl:text-[21px] 6xl:text-[31px] font-semibold text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px] pb-2">
              Fee/Cost:
            </p>
            {fee.map((fees) => (
              <p className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                {fees}
              </p>
            ))}
          </div>
          {/* who can participate */}
          <div className="pt-10 lg:pt-10 lg:px-24 2xl:px-28 6xl:px-32 space-y-2">
            <p className="text-[19px] md:text-[23px] lg:text-[19px] xl:text-[21px] 6xl:text-[31px] font-semibold text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px] pb-2">
              Who can Participate?
            </p>
            {participate.map((par) => (
              <p className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                {par}
              </p>
            ))}
          </div>
          {/* Resources for reading: */}
          <div className="pt-10 lg:pt-10 lg:px-24 2xl:px-28 6xl:px-32 space-y-2">
            <p className="text-[19px] md:text-[23px] lg:text-[19px] xl:text-[21px] 6xl:text-[31px] font-semibold text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px] pb-2">
              Resources for reading:
            </p>
            {resource.map((resources) => (
              <p className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                {resources}
              </p>
            ))}
          </div>
          {/* Relevant Research Studies: */}
          <div className="pt-10 lg:pt-10 lg:px-24 2xl:px-28 6xl:px-32 space-y-2">
            <p className="text-[19px] md:text-[23px] lg:text-[19px] xl:text-[21px] 6xl:text-[31px] font-semibold text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px] pb-2">
              Relevant Research Studies:
            </p>
            <ul className="flex flex-col">
              {research.map((researches) => (
                <li className="text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]">
                  {researches}
                </li>
              ))}
            </ul>
            <ul className="flex flex-col">
              {researchLink.map((link) => (
                <li key={link}>
                  {" "}
                  {/* Add a unique "key" prop for each list item */}
                  <a
                    href={link}
                    className="break-all text-[16px] md:text-[20px] lg:text-[16px] xl:text-[18px] 6xl:text-[28px] font-normal text-blue-500 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px]"
                  >
                    {link}
                  </a>
                </li>
              ))}
            </ul>
            <p className="text-[19px] md:text-[23px] lg:text-[19px] xl:text-[21px] 6xl:text-[31px] font-semibold text-gray-800 leading-8 md:leading-9 lg:leading-8 xl:leading-[38px] 6xl:leading-[44px] pb-2">
              {batchStart}
            </p>
            <form className="pt-2" id="payment"></form>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default ShoonyaCourse;