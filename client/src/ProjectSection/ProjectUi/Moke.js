import React from "react";
import Navbar from "../../Navbar/Navbar";
import Footer from "../../HomePage/Footer";

const Moke = () => {
  return (
    <div className="bg-white">
      <Navbar />
      <div className="bg-white px-5  md:pt-10 md:pb-3 md:px-10  lg:px-16 6xl:px-20 pt-10  lg:pt-20 5xl:pt-40">
        <div className="flex flex-row  justify-center items-center h-screen md:h-[500px]">
          <h1 className="text-3xl md:text-[45px] 2xl:text-4xl font-normal text-center  py-3 md:py-8 text-gray-800 tracking-wide  font-montserrat">
            COMING SOON
          </h1>
          
        </div>
        <Footer/>
      </div>
    </div>
  );
};

export default Moke;